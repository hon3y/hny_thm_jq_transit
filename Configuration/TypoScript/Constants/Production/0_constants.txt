plugin.tx_hive_thm_jq_transit {
    settings {
        production {
            includePath {
                public = EXT:hive_thm_jq_transit/Resources/Public/
                private = EXT:hive_thm_jq_transit/Resources/Private/
                frontend {
                    public = typo3conf/ext/hive_thm_jq_transit/Resources/Public/
                }
            }
            optional {
            	active = 0
            }
        }
    }
}