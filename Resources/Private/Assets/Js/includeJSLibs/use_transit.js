var hive_thm_jq_transit__interval = setInterval(function () {

    if (typeof jQuery == 'undefined') {
    }  else {

        clearInterval(hive_thm_jq_transit__interval);

        loadScript("/typo3conf/ext/hive_thm_jq_transit/Resources/Public/Assets/Js/transit.min.js.gzip?v=1", function () {
            if (hive_cfg_typoscript_sStage == "prototype" || hive_cfg_typoscript_sStage == "development") {
                console.info('jQuery transit loaded');
            }
        });
    }

}, 2000);

